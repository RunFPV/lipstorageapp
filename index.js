/**
 * @format
 */

import {AppRegistry} from 'react-native'
import App from './App'
import {name as appName} from './app.json'

import {LogBox} from 'react-native'

// Ignore non-serializable values from react-navigation
// Issue comes from realm objects, which are not properly serialized
LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state',
])

AppRegistry.registerComponent(appName, () => App)
