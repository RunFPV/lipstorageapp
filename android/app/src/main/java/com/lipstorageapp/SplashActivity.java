package com.lipstorageapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.devio.rn.splashscreen.SplashScreen;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String versionName = BuildConfig.VERSION_NAME;
        setContentView(R.layout.launch_screen);
        TextView versionTV = (TextView) findViewById(R.id.app_version);
        versionTV.setText(String.format("Version %s",versionName));

        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}