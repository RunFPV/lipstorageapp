/**
 * @format
 */

import 'react-native'
import React from 'react'
import App from '../App'
import Realm from 'realm'

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

beforeAll(async () => {
  jest.useFakeTimers()
})

it('renders correctly', async () => {
  renderer.create(<App />)
})

afterAll(async () => {
  Realm.clearTestState()
})
