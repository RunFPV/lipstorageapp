import React, {useState} from 'react'
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
  Text,
  Platform,
} from 'react-native'
// Components
import {UpdateBatterySchema} from '../schemaValidation'
import {CommonActions} from '@react-navigation/native'
import Form from '../components/Form'
import {useHeaderHeight} from '@react-navigation/elements'
import Toast from 'react-native-toast-message'
// Scripts
import {genProperties} from '../utils'
// Date
import moment from 'moment/min/moment-with-locales'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
// Database
import {addCellsInfo, updateBattery, updateCellsInfo} from '../database/realm'
import I18n from 'i18n-js'

const UpdateBatteryScreen = ({route, navigation}) => {
  const {id, battery, history, cellsNb, updateType} = route.params

  const style = useThemedStyles(styles)

  const verticalOffset = useHeaderHeight() + StatusBar.currentHeight

  const parsedCellsNb = parseInt(cellsNb, 10)

  const [loading, setloading] = useState(false)

  // Create Form from updateType
  const createForm = (type, batId) => {
    let form = {id: batId}
    switch (type) {
      case 'currentVoltage':
        form = {
          ...form,
          historyId: history?.id,
          ...getResVoltageFromCells(true, true),
        }
        break
      case 'lastResistance':
        form = {
          ...form,
          historyId: history?.id,
          ...getResVoltageFromCells(true, false, true),
        }
        break

      case 'chargedCapacity':
        form = {
          ...form,
          historyId: history?.id,
          chargedCapacity: history?.chargedCapacity?.toString(),
        }
        break

      case 'globalInfo':
        form = {
          ...form,
          brand: battery.brand,
          model: battery.model,
          totalCapacity: battery.totalCapacity.toString(),
          discharge: battery.discharge.toString(),
        }
        break

      default:
        form = {
          ...form,
          ...getResVoltageFromCells(false),
          chargedCapacity: history?.chargedCapacity?.toString(),
        }
    }

    return form
  }

  // TODO: Fonction à revoir (pue la merde)
  // Permet de faire ressortir les RES et VOLTAGE depuis des CELLS
  const getResVoltageFromCells = (withValues, withVoltage, withRes) => {
    if (withValues && history.cells?.length) {
      const listVoltage = Object.assign(
        ...history.cells.map(({index, voltage}) => {
          return {
            [`voltage${index}`]: voltage.toFixed(2),
          }
        }),
      )
      const listRes = Object.assign(
        ...history.cells.map(({index, res}) => {
          return {
            [`res${index}`]: res.toFixed(2),
          }
        }),
      )

      if (withVoltage && withRes) {
        return {...listVoltage, ...listRes}
      } else if (withVoltage) {
        return listVoltage
      } else if (withRes) {
        return listRes
      }
    }
    return {
      ...genProperties(parsedCellsNb, 'voltage', ''),
      ...genProperties(parsedCellsNb, 'res', ''),
    }
  }

  // TODO: Fonction à revoir (pue la merde)
  // Permet de faire ressortir les CELLS depuis des RES et VOLTAGE
  const fctKiPu = val => {
    let listRes = []
    Object.keys(val).forEach(key => {
      let res = {}
      if (key.includes('res')) {
        res.res = parseFloat(val[key])
        res.index = parseInt(key.slice(-1), 10)
        listRes.push(res)
      }
    })

    let listVoltage = []
    Object.keys(val).forEach(key => {
      let voltage = {}
      if (key.includes('voltage')) {
        voltage.voltage = parseFloat(val[key])
        voltage.index = parseInt(key.slice(-1), 10)
        listVoltage.push(voltage)
      }
    })

    const length = listRes?.length ? listRes.length : listVoltage?.length

    let finalList = []
    for (let i = 0; i < length; i++) {
      finalList.push({
        index: i,
        res: listRes[i]?.res,
        voltage: listVoltage[i]?.voltage,
      })
    }

    return finalList
  }

  const onFormSubmit = values => {
    setloading(true)
    // If updateType is not empty, it means it's an update and not a new histoInfo
    if (updateType) {
      // globalInfo Update
      if (updateType === 'globalInfo') {
        updateBattery(values)
          .then(() => navigation.goBack())
          // Print Error
          .catch(error => {
            Toast.show({type: 'error', text1: 'Error', text2: error.message})
            setloading(false)
          })
      } else {
        updateCellsInfo({...values, cells: fctKiPu(values)})
          .then(() => navigation.goBack())
          // Print Error
          .catch(error => {
            Toast.show({type: 'error', text1: 'Error', text2: error.message})
            setloading(false)
          })
      }
    } else {
      addCellsInfo({...values, cells: fctKiPu(values)})
        .then(() => {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {name: 'main'},
                {
                  name: 'BatteryInfo',
                  params: {id: id},
                },
              ],
            }),
          )
        })
        // Print Error
        .catch(error => {
          Toast.show({type: 'error', text1: 'Error', text2: error.message})
          setloading(false)
        })
    }
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={verticalOffset}
      style={style.keyBoardContainer}>
      <ScrollView style={style.container}>
        <Form
          onFormSubmit={onFormSubmit}
          validationSchema={UpdateBatterySchema(parsedCellsNb)}
          formValues={createForm(updateType, id)}
          loading={loading}
        />
        {updateType && (
          <Text style={style.lastUpdateDate}>
            {`${I18n.t('updateBattery.updatedDate')} ${moment(
              updateType === 'globalInfo'
                ? battery.updatedDate
                : history?.updatedDate,
            ).format('LLL')}`}
          </Text>
        )}
      </ScrollView>
    </KeyboardAvoidingView>
  )
}

const styles = theme =>
  StyleSheet.create({
    keyBoardContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    container: {
      paddingHorizontal: theme.spacing.margin.S,
      marginBottom: theme.spacing.margin.S,
    },
    lastUpdateDate: {
      color: theme.colors.TEXT,
      padding: theme.spacing.margin.S,
    },
  })

export default UpdateBatteryScreen
