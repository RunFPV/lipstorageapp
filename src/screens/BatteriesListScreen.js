import React, {
  useState,
  useLayoutEffect,
  useCallback,
  useEffect,
  useContext,
} from 'react'
import {Text, StyleSheet, View, FlatList} from 'react-native'
// Components
import {useFocusEffect} from '@react-navigation/native'
import TouchableIcon from '../components/TouchableIcon'
import {faPlus} from '@fortawesome/free-solid-svg-icons'
import Separator from '../components/Separator'
import BatteriesListItem from '../components/BatteriesListItem'
import {UserContext} from '../contexts/User'
// Scripts
import {calculAverageCharge} from '../utils'
import {manageNotification} from '../utils/notification'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
// Internationalization
import I18n from '../i18n'
// Database
import {getBatteries} from '../database/realm'

const BatteriesListScreen = ({navigation}) => {
  const style = useThemedStyles(styles)

  const [batteries, setbatteries] = useState(getBatteries())
  const [refreshing, setrefreshing] = useState(false)

  const [state] = useContext(UserContext)

  const onRefresh = () => {
    setrefreshing(true)
    setbatteries(getBatteries())
    setrefreshing(false)
  }

  useEffect(
    () => manageNotification(batteries, state.rollingNotificationDelay),
    [batteries, state.rollingNotificationDelay],
  )

  // Plus button in Header Right
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableIcon
          icon={faPlus}
          onPress={() => navigation.navigate('InfosWithCamera', {isOCR: false})}
        />
      ),
      // TODO: Suppression des batteries
      // headerLeft: () => (
      //   <TouchableIcon icon={} onPress={() => alert('Coming soon...')} />
      // ),
    })
  }, [navigation])

  // Update data onFocus
  useFocusEffect(
    useCallback(() => {
      setbatteries(getBatteries())
    }, []),
  )

  return (
    <View style={style.container}>
      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        contentContainerStyle={[
          batteries.length < 1 && style.center,
          style.listContainer,
        ]}
        data={batteries}
        renderItem={({item}) => (
          <BatteriesListItem
            key={item.id}
            id={item.id}
            averageVoltage={calculAverageCharge(item.history[0]?.cells)}
            brand={item.brand}
            model={item.model}
            onPress={() => navigation.navigate('BatteryInfo', {id: item.id})}
          />
        )}
        ItemSeparatorComponent={Separator}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={
          <View style={style.center}>
            <Text style={style.emptyListText}>
              {I18n.t('batteriesList.emptyList')}
            </Text>
          </View>
        }
      />
    </View>
  )
}

const styles = theme =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    listContainer: {
      margin: theme.spacing.margin.S,
    },
    center: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
    },
    emptyListText: {
      color: theme.colors.TEXT,
      textAlign: 'center',
      fontSize: theme.typography.size.M,
    },
  })

export default BatteriesListScreen
