import React, {useContext, useState, useLayoutEffect, useCallback} from 'react'
import {StyleSheet, ScrollView, Text, View, Alert} from 'react-native'
// Components
import {useFocusEffect} from '@react-navigation/native'
import {
  faBatteryFull,
  faBatteryQuarter,
  faHeart,
  faHeartbeat,
  faHistory,
  faInfo,
  faSyncAlt,
  faCircle,
  faBolt,
  faCarBattery,
  faChargingStation,
  faEdit,
  faTrash,
  faClock,
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import Separator from '../components/Separator'
import TextButton from '../components/TextButton'
import {UserContext} from '../contexts/User'
import Card from '../components/Card'
import CardItem from '../components/CardItem'
import TouchableIcon from '../components/TouchableIcon'
import {Toast} from 'react-native-toast-message/lib/src/Toast'
// Scripts
import {calculHealth, displayBatteryIcon, calculAverageCharge} from '../utils'
// Date
import moment from 'moment/min/moment-with-locales'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'
// Database
import {getBattery, deleteBattery} from '../database/realm'

const BatteryInfoScreen = ({route, navigation}) => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  const [battery, setbattery] = useState(getBattery(route.params.id))
  const [state] = useContext(UserContext)

  // Plus button in Header Right
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableIcon
          icon={faEdit}
          onPress={() =>
            navigation.navigate('UpdateBatteryNoModal', {
              id: battery.id,
              battery: battery,
              updateType: 'globalInfo',
            })
          }
        />
      ),
    })
  }, [battery, navigation])

  // Update data onFocus
  useFocusEffect(
    useCallback(() => {
      setbattery(getBattery(route.params.id))
    }, [route.params.id]),
  )

  //TODO : Create a custom AlertConf component
  const onClicDeleteBattery = id => {
    Alert.alert(
      I18n.t('alert.confirmationTitle'),
      I18n.t('alert.confirmationText'),
      [
        // The "Yes" button
        {
          text: I18n.t('alert.yes'),
          onPress: () => {
            deleteBattery(id)
              .then(() => navigation.goBack())
              // Print Error
              .catch(error =>
                Toast.show({
                  type: 'error',
                  text1: 'Error',
                  text2: error.message,
                }),
              )
          },
        },
        // The "No" button
        // Does nothing but dismiss the dialog when tapped
        {
          text: I18n.t('alert.no'),
        },
      ],
    )
  }

  const onCardClicUpdate = card => {
    if (battery.history?.length) {
      navigation.navigate('UpdateBatteryNoModal', {
        id: battery.id,
        history: battery.history[0],
        updateType: card,
      })
    } else {
      Alert.alert(I18n.t('alert.noHistoryTitle'), I18n.t('alert.noHistoryText'))
    }
  }

  return (
    <ScrollView>
      <View style={style.container}>
        <View style={style.center}>
          {/* change battery icon in terms of battery voltage */}
          <FontAwesomeIcon
            icon={displayBatteryIcon(
              calculAverageCharge(battery.history[0]?.cells),
            )}
            iconSize={15}
            size={100}
            color={theme.colors.TEXT}
          />
          <Text style={style.title}>{battery.id}</Text>
          <Text style={style.subTitle}>
            ({battery.model} - {battery.brand})
          </Text>
        </View>

        <Separator size="S" />

        <CardItem
          styleContainer={style.healthCard}
          iconSize={15}
          icon={faHeart}
          text={`${I18n.t('batteryInfo.battery.globalHealth')} : ${
            battery.history?.length && battery.cycles
              ? (
                  calculHealth(
                    battery.history[0].cells,
                    battery.cycles,
                    battery.createdDate,
                    state.battery,
                  ) * 100
                ).toFixed(0) + ' %'
              : I18n.t('batteryInfo.insufficientData')
          }`}
        />

        <Separator />

        <Card icon={faInfo} color={theme.colors.WARNING} title="Info">
          <View style={style.cardContentCol}>
            <CardItem
              icon={faSyncAlt}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.cycles')} : ${
                battery.cycles
              }`}
            />
            <CardItem
              icon={faBatteryQuarter}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.minCapacityThreshold')} : ${(
                battery.totalCapacity *
                (1 - state.battery.minCapacityThreshold)
              ).toFixed(2)} mAh`}
            />
            <CardItem
              icon={faBatteryFull}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.fullCapacity')} : ${
                battery.totalCapacity
              } mAh`}
            />
            <CardItem
              icon={faCarBattery}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.cellsNumber')} : ${
                battery.cellsNb
              }`}
            />
            <CardItem
              icon={faChargingStation}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.maxChargeRate')} : ${(
                battery.totalCapacity *
                state.battery.chargeRate *
                0.001
              ).toFixed(2)} A`}
            />
            <CardItem
              icon={faClock}
              iconSize={15}
              centerContent
              text={`${I18n.t('batteryInfo.battery.age')} : ${moment(
                battery.createdDate,
              ).fromNow(true)}`}
            />
          </View>
        </Card>
        <Separator />
        <Card
          icon={faChargingStation}
          color={theme.colors.TERTIARY}
          title={I18n.t('batteryInfo.battery.chargedCapacity')}
          onPress={() => onCardClicUpdate('chargedCapacity')}>
          <View style={[style.cardContent, style.cardContentCenter]}>
            <CardItem
              text={
                battery.history[0]?.chargedCapacity
                  ? `${battery.history[0].chargedCapacity} mAh`
                  : I18n.t('batteryInfo.noDataFound')
              }
            />
          </View>
        </Card>
        <Separator />
        <Card
          icon={faBolt}
          title={I18n.t('batteryInfo.battery.currentVoltage')}
          onPress={() => onCardClicUpdate('currentVoltage')}>
          <View style={style.cardContent}>
            {battery.history?.length ? (
              battery.history[0].cells.map((cell, index) => {
                return (
                  <CardItem
                    key={index}
                    icon={faCircle}
                    text={`${cell.voltage.toFixed(2)} v`}
                  />
                )
              })
            ) : (
              <Text style={style.noDataFoundText}>
                {I18n.t('batteryInfo.noDataFound')}
              </Text>
            )}
          </View>
        </Card>
        <Separator />
        <Card
          icon={faHeartbeat}
          title={I18n.t('batteryInfo.battery.lastResistance')}
          color={theme.colors.SUCCESS}
          onPress={() => onCardClicUpdate('lastResistance')}>
          <View style={style.cardContent}>
            {battery.history?.length ? (
              battery.history[0].cells.map((cell, index) => {
                return (
                  <CardItem
                    key={index}
                    icon={faCircle}
                    text={`${cell.res.toFixed(2)} mΩ`}
                  />
                )
              })
            ) : (
              <Text style={style.noDataFoundText}>
                {I18n.t('batteryInfo.noDataFound')}
              </Text>
            )}
          </View>
        </Card>
        <Separator />
        <TextButton
          text={I18n.t('batteryInfo.viewHistory')}
          icon={faHistory}
          color={theme.colors.SECONDARY}
          onPress={() => navigation.navigate('History', {id: battery.id})}
        />
        <Separator />
        <TextButton
          text={I18n.t('batteryInfo.delete')}
          icon={faTrash}
          color={theme.colors.ERROR}
          onPress={() => onClicDeleteBattery(battery.id)}
        />
      </View>
    </ScrollView>
  )
}

const styles = theme =>
  StyleSheet.create({
    container: {
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.margin.S,
      marginBottom: theme.spacing.margin.S,
    },
    title: {
      color: theme.colors.TEXT,
      fontSize: theme.typography.size.M,
    },
    subTitle: {
      color: theme.colors.TEXT,
      fontSize: theme.typography.size.s,
    },
    cardContentCol: {
      alignItems: 'flex-start',
      flex: 1,
    },
    cardContent: {
      flexDirection: 'row',
      alignItems: 'center',
      flex: 1,
      flexWrap: 'wrap',
      flexBasis: '50%',
    },
    cardContentCenter: {
      justifyContent: 'center',
    },
    noDataFoundText: {
      flex: 1,
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.s,
      textAlign: 'center',
    },
    healthCard: {
      backgroundColor: theme.colors.ERROR,
      padding: theme.spacing.margin.S,
      fontSize: theme.typography.size.s,
    },
    center: {
      alignItems: 'center',
    },
  })

export default BatteryInfoScreen
