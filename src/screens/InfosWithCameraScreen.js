import React, {
  useCallback,
  useEffect,
  useState,
  useRef,
  useLayoutEffect,
} from 'react'
import {Text, StyleSheet, View, ActivityIndicator, Platform} from 'react-native'
// Components
import {HeaderBackButton} from '@react-navigation/elements'
import {Camera, useCameraDevices} from 'react-native-vision-camera'
import {useFocusEffect} from '@react-navigation/native'
import {BarcodeFormat, useScanBarcodes} from 'vision-camera-code-scanner'
import 'react-native-reanimated'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
// Internationalization
import I18n from '../i18n'
// Database
import {getBattery} from '../database/realm'

const InfosWithCameraScreen = ({navigation, route}) => {
  const style = useThemedStyles(styles)

  const [cameraActive, setcameraActive] = useState(true)
  const [cameraPermissionStatus, setcameraPermissionStatus] =
    useState('not-determined')
  const devices = useCameraDevices('wide-angle-camera')
  const device = devices.back

  const [camLocation, setcamLocation] = useState()
  const [removeframeProc, setremoveframeProc] = useState(false)

  const cameraRef = useRef(null)

  const [frameProcessor, detectedData] = useScanBarcodes([
    BarcodeFormat.ALL_FORMATS,
  ])

  useEffect(() => {
    Camera.requestCameraPermission().then(permission =>
      setcameraPermissionStatus(permission),
    )
  }, [])

  useEffect(() => {
    const onDetectedData = () => {
      // If id already existing in database, go directly to UpdateBattery
      // else go to AddBattery

      // Remove invalid characters displayed on Android
      const regex = /[^A-Za-z0-9]/g
      const battId = detectedData[0].displayValue.replace(regex, '')

      let navParams = {id: battId, cellsNb: ''}
      let navTo = 'AddBattery'
      const battery = getBattery(battId)
      if (battery) {
        navParams.cellsNb = battery.cellsNb
        navTo = 'UpdateBattery'
      }

      //Deactivate camera onDetectedData
      setcameraActive(false)
      setremoveframeProc(true)
      navigation.navigate(navTo, navParams)
    }
    detectedData.length > 0 && onDetectedData()
  }, [detectedData, navigation])

  // Reactivate camera onFocus
  useFocusEffect(
    useCallback(() => {
      setcameraActive(true)
      setremoveframeProc(false)
    }, []),
  )

  // Override GoBack method, to de activate FrameProc for android
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: props => (
        <HeaderBackButton
          {...props}
          labelVisible={Platform.OS === 'ios'}
          label={I18n.t('global.back')}
          onPress={() => {
            setcameraActive(false)
            setremoveframeProc(true)
            navigation.goBack()
          }}
        />
      ),
    })
  }, [navigation])

  const onTouchToFocus = async touchEvent => {
    let point = {
      x: Math.round(touchEvent.pageX - camLocation.x),
      y: Math.round(touchEvent.pageY - camLocation.y),
    }
    await cameraRef?.current?.focus(point)
  }

  return (
    <View style={style.container}>
      {cameraPermissionStatus === 'authorized' ? (
        device ? (
          <Camera
            ref={cameraRef}
            style={style.cameraPreview}
            device={device}
            isActive={cameraActive}
            frameProcessor={removeframeProc ? null : frameProcessor}
            frameProcessorFps="auto"
            onLayout={event => {
              const layout = event.nativeEvent.layout
              setcamLocation({x: layout.x, y: layout.y})
            }}
            onTouchEnd={async x =>
              device.supportsFocus && (await onTouchToFocus(x.nativeEvent))
            }
          />
        ) : (
          <ActivityIndicator style={style.loader} />
        )
      ) : (
        <Text style={style.text}>{I18n.t('infoWithCamera.unauthorized')}</Text>
      )}
    </View>
  )
}

const styles = theme =>
  StyleSheet.create({
    cameraPreview: {
      flex: 1,
    },
    container: {
      flex: 1,
    },
    text: {},
    loader: {
      flex: 1,
    },
    button: {
      margin: 10,
      position: 'absolute',
      bottom: 0,
      left: 0,
      right: 0,
    },
  })

export default InfosWithCameraScreen
