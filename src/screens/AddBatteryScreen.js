import React, {useState} from 'react'
import {
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  StatusBar,
  Platform,
} from 'react-native'
// Components
import {AddBatterySchema} from '../schemaValidation'
import {useHeaderHeight} from '@react-navigation/elements'
import {CommonActions} from '@react-navigation/native'
import Form from '../components/Form'
import Toast from 'react-native-toast-message'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
// Database
import {addBattery} from '../database/realm'

const AddBatteryScreen = ({route, navigation}) => {
  const id = route.params?.id

  const style = useThemedStyles(styles)

  const verticalOffset = useHeaderHeight() + StatusBar.currentHeight

  const [loading, setloading] = useState(false)

  const formValues = {
    id: id,
    brand: '',
    model: '',
    cellsNb: '',
    totalCapacity: '',
    discharge: '',
  }

  const onFormSubmit = values => {
    setloading(true)
    // Add in local db
    addBattery(values)
      .then(() => {
        // routing to updateBattery with id (QRCode) et cells Nb.
        // reset routes to disable the possibility to back to AddBatteryScreen
        // when coming from this last
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              {name: 'main'},
              {
                name: 'UpdateBattery',
                params: {id: values.id, cellsNb: values.cellsNb},
              },
            ],
          }),
        )
      })
      // Print Error
      .catch(error => {
        Toast.show({type: 'error', text1: 'Error', text2: error.message})
        setloading(false)
      })
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={verticalOffset}
      style={style.keyBoardContainer}>
      <ScrollView style={style.container}>
        <Form
          onFormSubmit={onFormSubmit}
          validationSchema={AddBatterySchema}
          formValues={formValues}
          loading={loading}
        />
      </ScrollView>
    </KeyboardAvoidingView>
  )
}

const styles = theme =>
  StyleSheet.create({
    keyBoardContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
    },
    container: {
      paddingHorizontal: theme.spacing.margin.S,
      marginBottom: theme.spacing.margin.S,
    },
  })

export default AddBatteryScreen
