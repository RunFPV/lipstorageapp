import React, {useState, useContext} from 'react'
import {StyleSheet, View} from 'react-native'
// Components
import SettingsList from 'react-native-settings-list'
import {
  faBatteryQuarter,
  faBell,
  faCalendarAlt,
  faChargingStation,
  faGlobeEurope,
  faHeartbeat,
  faSyncAlt,
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {UserContext} from '../contexts/User'
import ModalPicker from '../components/ModalPicker'
import * as a from '../contexts/User/actions'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'
import en from '../i18n/locales/en'

const SettingsScreen = ({navigation}) => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  const [state, dispatch] = useContext(UserContext)

  const [showModal, setshowModal] = useState(false)
  const [pickerData, setpickerData] = useState([])
  const [dispatchType, setdispatchType] = useState('')
  const [currentItemValue, setcurrentItemValue] = useState()

  const displayModal = (pickerItems, itemValue, dispatchAction) => {
    setpickerData(pickerItems)
    setcurrentItemValue(itemValue)
    setdispatchType(dispatchAction)
    setshowModal(true)
  }

  const save = itemValue => {
    dispatch({type: dispatchType, value: itemValue})
    setshowModal(false)
  }

  const dataPicker = {
    language: Object.entries(en.language).map(item => {
      return {label: I18n.t(`language.${item[0]}`), value: item[0]}
    }),
    rollingNotificationDelay: Array(Math.ceil((7 + 7 / 2) * 24)) // about 250 days (1 week and a half)
      .fill(0)
      .map((item, index) => {
        const value = index + 1
        return {label: value.toFixed(0), value: value}
      }),
    battery: {
      // TODO: Refacto via une fonction commune dans utils
      minCapacityThreshold: Array(99)
        .fill(0)
        .map((item, index) => {
          const value = (index + 1) / 100
          return {label: value.toFixed(2), value: value}
        }),
      chargeRate: Array(60)
        .fill(0)
        .map((item, index) => {
          const value = (index + 1) / 10
          return {label: value.toFixed(1), value: value}
        }),
      maxResThreshold: Array(2500)
        .fill(0)
        .map((item, index) => {
          const value = (index + 1) / 100
          return {label: value.toFixed(2), value: value}
        }),
      maxDayThreshold: Array(Math.ceil(365.25 * 3.6)) // about 1300 days (3 years and a half)
        .fill(0)
        .map((item, index) => {
          const value = index + 1
          return {label: value.toFixed(0), value: value}
        }),
      maxCycleThreshold: Array(800)
        .fill(0)
        .map((item, index) => {
          const value = index + 1
          return {label: value.toFixed(0), value: value}
        }),
    },
  }

  return (
    <View style={style.container}>
      <ModalPicker
        isVisible={showModal}
        toggle={() => setshowModal(!showModal)}
        items={pickerData}
        currentItemValue={currentItemValue}
        save={save}
      />
      <SettingsList borderColor={theme.colors.BACKGROUND}>
        {/* TODO: Unable to create a custom component of SettingsList.Item. Due to dot component ? */}

        <SettingsList.Header
          headerText={I18n.t('settings.headerList.global')}
          headerStyle={style.headerStyle}
        />

        {/* Language */}
        <SettingsList.Item
          title={I18n.t('settings.language')}
          titleInfo={I18n.t(`language.${state.language}`)}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={style.itemBoxStyle}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(dataPicker.language, state.language, a.UPDATE_LANGUAGE)
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon
                icon={faGlobeEurope}
                color={theme.colors.INVERSED}
              />
            </View>
          }
        />

        {/* RollingNotificationDelay */}
        <SettingsList.Item
          title={I18n.t('settings.rollingNotificationDelay')}
          titleInfo={state.rollingNotificationDelay.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={[style.itemBoxStyle, style.lastItemStyle]}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.rollingNotificationDelay,
              state.rollingNotificationDelay,
              a.UPDATE_ROLLING_NOTIFICATION_DELAY,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon icon={faBell} color={theme.colors.INVERSED} />
            </View>
          }
        />

        <SettingsList.Header
          headerText={I18n.t('settings.headerList.battery')}
          headerStyle={style.headerStyle}
        />

        {/* MinCapacityThreshold */}
        <SettingsList.Item
          title={I18n.t('settings.battery.minCapacityThreshold')}
          titleInfo={state.battery.minCapacityThreshold.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={style.itemBoxStyle}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.battery.minCapacityThreshold,
              state.battery.minCapacityThreshold,
              a.BATTERY.UPDATE_MIN_CAPACITY_THRESHOLD,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon
                icon={faBatteryQuarter}
                color={theme.colors.INVERSED}
              />
            </View>
          }
        />

        {/* MaxResThreshold */}
        <SettingsList.Item
          title={I18n.t('settings.battery.maxResThreshold')}
          titleInfo={state.battery.maxResThreshold.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={style.itemBoxStyle}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.battery.maxResThreshold,
              state.battery.maxResThreshold,
              a.BATTERY.UPDATE_MAX_RES_THRESHOLD,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon
                icon={faHeartbeat}
                color={theme.colors.INVERSED}
              />
            </View>
          }
        />

        {/* MaxCycleThreshold */}
        <SettingsList.Item
          title={I18n.t('settings.battery.maxCycleThreshold')}
          titleInfo={state.battery.maxCycleThreshold.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={style.itemBoxStyle}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.battery.maxCycleThreshold,
              state.battery.maxCycleThreshold,
              a.BATTERY.UPDATE_MAX_CYCLE_THRESHOLD,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon icon={faSyncAlt} color={theme.colors.INVERSED} />
            </View>
          }
        />

        {/* MaxDayThreshold */}
        <SettingsList.Item
          title={I18n.t('settings.battery.maxDayThreshold')}
          titleInfo={state.battery.maxDayThreshold.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={style.itemBoxStyle}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.battery.maxDayThreshold,
              state.battery.maxDayThreshold,
              a.BATTERY.UPDATE_MAX_DAY_THRESHOLD,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon
                icon={faCalendarAlt}
                color={theme.colors.INVERSED}
              />
            </View>
          }
        />

        {/* ChargeRate */}
        <SettingsList.Item
          title={I18n.t('settings.battery.chargeRate')}
          titleInfo={state.battery.chargeRate.toString()}
          hasNavArrow={false}
          rightSideStyle={style.rightSideStyle}
          itemBoxStyle={[style.itemBoxStyle, style.lastItemStyle]}
          titleBoxStyle={style.titleBoxStyle}
          titleStyle={style.titleStyle}
          onPress={() =>
            displayModal(
              dataPicker.battery.chargeRate,
              state.battery.chargeRate,
              a.BATTERY.UPDATE_CHARGE_RATE,
            )
          }
          icon={
            <View style={style.centerIcon}>
              <FontAwesomeIcon
                icon={faChargingStation}
                color={theme.colors.INVERSED}
              />
            </View>
          }
        />
      </SettingsList>
    </View>
  )
}

const styles = theme =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    rightSideStyle: {
      paddingRight: theme.spacing.margin.M,
      alignSelf: 'center',
    },
    itemBoxStyle: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: theme.colors.BACKGROUND,
      borderBottomWidth: 0.5,
      borderColor: theme.colors.TEXT,
    },
    lastItemStyle: {
      borderBottomWidth: 0,
    },
    titleBoxStyle: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-around',
      margin: 0,
    },
    titleStyle: {
      color: theme.colors.INVERSED,
    },
    centerIcon: {
      justifyContent: 'center',
      alignItems: 'center',
      padding: theme.spacing.margin.M,
    },
    headerStyle: {
      color: theme.colors.INVERSED,
      fontSize: theme.typography.size.M,
      padding: theme.spacing.margin.XS,
    },
  })

export default SettingsScreen
