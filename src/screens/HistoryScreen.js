import React, {useContext, useState, useCallback} from 'react'
import {StyleSheet, Text, View, Dimensions, ScrollView} from 'react-native'
// Components
import {useFocusEffect} from '@react-navigation/native'
import {LineChart} from 'react-native-chart-kit'
import {UserContext} from '../contexts/User'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'
// Database
import {getBattery} from '../database/realm'

const HistoryScreen = ({route}) => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  const [battery, setbattery] = useState(getBattery(route.params.id))
  const [state] = useContext(UserContext)

  const chartWidth = () => {
    const chartLength =
      (getDataSets().labels.length * Dimensions.get('window').width) / 5
    return chartLength < Dimensions.get('window').width
      ? Dimensions.get('window').width
      : chartLength
  }

  // Update data onFocus
  useFocusEffect(
    useCallback(() => {
      setbattery(getBattery(route.params.id))
    }, [route.params.id]),
  )

  const getDataSets = () => {
    let datasets = []
    const labels = []
    const legend = []

    battery.history
      .slice()
      .reverse()
      .forEach(hist => {
        labels.push(hist.updatedDate.toLocaleDateString(state.language))
        // Foreach cells, get res
        for (let i = 0; i < hist.cells.length; i++) {
          if (!datasets[`data${i}`]) {
            legend.push(`${I18n.t('history.cell')} ${i + 1}`)
            datasets[`data${i}`] = []
          }
          datasets[`data${i}`].push(hist.cells[i].res)
        }
      })

    datasets = Object.keys(datasets).map((key, index) => {
      // a voir pour randomiser + ajouter des couleurs spéciales
      const colors = Object.keys(theme.colors)
      return {
        data: datasets[key],
        color: () => theme.colors[colors[index]],
      }
    })

    return {datasets, labels, legend}
  }

  return (
    <View style={style.container}>
      {battery.history.length ? (
        <View>
          <Text style={style.title}>
            {I18n.t('history.resistanceOverTime')}
          </Text>
          <ScrollView horizontal>
            <LineChart
              data={getDataSets()}
              width={chartWidth()}
              yLabelsOffset={6}
              height={Dimensions.get('window').height / 3}
              yAxisSuffix="mΩ"
              yAxisInterval={1}
              chartConfig={{
                useShadowColorFromDataset: true,
                backgroundGradientFrom: theme.colors.BACKGROUND,
                backgroundGradientTo: theme.colors.BACKGROUND,
                color: () => theme.colors.TEXT,
                style: {
                  borderRadius: 16,
                },
                strokeWidth: 2,
                propsForDots: {
                  r: '4',
                  strokeWidth: '2',
                  stroke: theme.colors.INVERSED,
                },
              }}
              style={{
                margin: theme.spacing.margin.S,
                borderRadius: theme.spacing.radius.L,
              }}
            />
          </ScrollView>
        </View>
      ) : (
        <Text style={style.noDataFoundText}>
          {I18n.t('history.noHistoryFound')}
        </Text>
      )}
    </View>
  )
}

const styles = theme =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    title: {
      color: theme.colors.INVERSED,
      fontSize: theme.typography.size.M,
      marginHorizontal: theme.spacing.margin.S,
      marginTop: theme.spacing.margin.S,
    },
    noDataFoundText: {
      marginTop: theme.spacing.margin.S,
      color: theme.colors.TEXT,
      fontSize: theme.typography.size.M,
      textAlign: 'center',
    },
  })

export default HistoryScreen
