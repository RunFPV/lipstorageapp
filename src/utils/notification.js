// Componenents
import {Notifications} from 'react-native-notifications'
// Scripts
import {calculAverageCharge} from '.'
// Internationalization
import I18n from '../i18n' // remove ca, refacto de la classe utils
// Date
import moment from 'moment/min/moment-with-locales'
// Database
import {updateBattery} from '../database/realm'

const manageNotification = (batteries, hours) => {
  batteries.forEach(battery => {
    const currentDate = moment()
    const parsedVoltage = parseFloat(
      calculAverageCharge(battery.history[0]?.cells),
    )

    // < 3.75 ou > 3.85
    if (parsedVoltage < 3.75 || parsedVoltage > 3.85) {
      const content = `${
        parsedVoltage < 3.7
          ? `${I18n.t('notification.content.lowVoltage')}`
          : `${I18n.t('notification.content.highVoltage')}`
      } ${I18n.t('notification.content.since')} ${moment(
        battery.history[0]?.updatedDate,
      ).fromNow(true)}`

      // If no notif subscribed
      // or there is cells update(s) and average voltage is always bad
      // or notification already notified and average voltage still not good
      if (
        battery.notificationId === null ||
        battery.notificationDate < battery.history[0]?.updatedDate ||
        battery.notificationDate < currentDate.toDate()
      ) {
        const notificationId = Notifications.postLocalNotification({
          body: content,
          title: `${I18n.t('notification.title')} ${battery.id}`,
          // silent: false,
          // userInfo: {},
          fireDate: currentDate.add(hours, 'hours').toISOString(),
        })

        updateBattery({
          id: battery.id,
          notification: {
            id: notificationId,
            date: currentDate.toDate(),
          },
        })
      }
    } else {
      // Remove notification if average cells  votlage is god
      if (battery.notificationId !== null) {
        Notifications.cancelLocalNotification(battery.notificationId)
        updateBattery({
          id: battery.id,
          notification: {
            notificationId: null,
            notificationdate: null,
          },
        })
      }
    }
  })
}

export {manageNotification}
