// Componenents
import {
  faBatteryFull,
  faBatteryHalf,
  faBatteryQuarter,
  faBatteryEmpty,
  faBatteryThreeQuarters,
} from '@fortawesome/free-solid-svg-icons'

// Generate a list of object properties with incrementValue
const genProperties = (size, propName, propValue, incrementPropValue) => {
  let array = []
  for (let i = 0; i < size; i++) {
    const value =
      propValue && incrementPropValue ? `${propValue} ${i + 1}` : propValue
    array.push({[`${propName}${i}`]: value})
  }

  return array.length ? Object.assign(...array) : null
}

// Calcul battery's cycles with totalCapacity and historyChargedCapacity
const calculCycles = (history, totalCapacity) => {
  if (history?.length) {
    const sumHistoryCharged = history.reduce((accu, item) => {
      return {chargedCapacity: accu.chargedCapacity + item.chargedCapacity}
    }).chargedCapacity

    return Math.floor(sumHistoryCharged / totalCapacity)
  } else {
    return 0
  }
}

// Calcul battery's global Health with cycle number, internal resistance and age
const calculHealth = (
  cells,
  cycle,
  createdDate,
  {maxCycleThreshold, maxResThreshold, maxDayThreshold},
) => {
  // get current date
  const today = new Date()
  // convert day to ms
  const oneDay = 1000 * 60 * 60 * 24
  // time diff between created date and today
  const diffTime = createdDate.getTime() - today.getTime()
  // get diff days
  const aged = Math.round(diffTime / oneDay)

  // get max cells internal resistance (mOhm)
  const maxRes = Math.max(
    ...cells.map(cell => {
      return cell.res
    }),
  )

  const resRate = maxRes / maxResThreshold
  const dateRate = aged / maxDayThreshold
  const cycleRate = cycle / maxCycleThreshold

  return (
    1 -
    averageWithWeight([
      {value: resRate, weight: 10},
      {value: dateRate, weight: 2},
      {value: cycleRate, weight: 1},
    ])
  )
}

// Get the average with weight
const averageWithWeight = valuesList => {
  let totalWeight = 0
  let values = 0
  valuesList.forEach(item => {
    values += item.value * item.weight
    totalWeight += item.weight
  })

  return values / totalWeight
}

// Calcul average charge with cells
const calculAverageCharge = cells => {
  if (cells?.length) {
    const average =
      cells.reduce((prev, curr) => {
        return {voltage: prev.voltage + curr.voltage}
      }).voltage / cells.length
    return average.toFixed(2)
  }
  return '0'
}

// Display battery icon in terms of battery level
const displayBatteryIcon = batteryLevel => {
  const batteryIcon = {
    empty: faBatteryEmpty,
    quarter: faBatteryQuarter,
    half: faBatteryHalf,
    threeQuarters: faBatteryThreeQuarters,
    full: faBatteryFull,
  }

  if (batteryLevel < 3.73) {
    return batteryIcon.empty
  } else if (batteryLevel < 3.8) {
    return batteryIcon.quarter
  } else if (batteryLevel < 3.87) {
    return batteryIcon.half
  } else if (batteryLevel < 4.02) {
    return batteryIcon.threeQuarters
  } else {
    return batteryIcon.full
  }
}

export {
  displayBatteryIcon,
  calculHealth,
  genProperties,
  calculCycles,
  calculAverageCharge,
}
