// TODO: revoir l'implementation de realm, sur plusieurs fichiers peut etre ?
// voir bonnes pratiques mongooooooze
// https://docs.mongodb.com/realm/sdk/react-native/data-types/field-types/

// voir si les promises sont interessantes à utiliser
import Realm from 'realm'
import {calculCycles} from '../utils'

class History extends Realm.Object {}
History.schema = {
  name: 'History',
  embedded: true, // default: false
  properties: {
    id: 'int',
    cells: 'Cell[]',
    chargedCapacity: 'int?',
    createdDate: 'date',
    updatedDate: 'date',
  },
}

class Cell extends Realm.Object {}
Cell.schema = {
  name: 'Cell',
  embedded: true, // default: false
  properties: {
    index: 'int',
    res: 'float',
    voltage: 'float',
  },
}

class Battery extends Realm.Object {}
Battery.schema = {
  name: 'Battery',
  properties: {
    id: 'string',
    brand: 'string',
    model: 'string',
    cellsNb: 'int',
    totalCapacity: 'int',
    discharge: 'int',
    history: 'History[]',
    notificationId: 'int?',
    notificationDate: 'date?',
    createdDate: 'date',
    updatedDate: 'date',
  },
  primaryKey: 'id',
}

const realm = new Realm({
  schema: [Battery, Cell, History],
  deleteRealmIfMigrationNeeded: true,
  // deleteRealmIfMigrationNeeded: true,
})

export default realm

const getBatteries = () => {
  const batteries = realm.objects('Battery')
  return batteries
}

const getBattery = id => {
  const battery = realm.objects('Battery').filtered(`id == '${id}'`)[0]
  if (battery) {
    battery.cycles = calculCycles(battery.history, battery.totalCapacity)
  }

  return battery
}

const addCellsInfo = ({id, cells, chargedCapacity}) => {
  return new Promise((resolve, reject) => {
    try {
      const currentDate = new Date()
      const battery = realm.objects('Battery').filtered(`id == '${id}'`)[0]

      if (!battery) {
        throw Error('Battery not found')
      }

      let lastHistId = battery.history?.length
        ? battery.history[battery.history.length - 1].id
        : 0

      realm.write(() => {
        resolve(
          battery.history.unshift({
            id: lastHistId + 1,
            chargedCapacity: chargedCapacity && parseInt(chargedCapacity, 10),
            cells: cells,
            createdDate: currentDate,
            updatedDate: currentDate,
          }),
        )
      })
    } catch (error) {
      reject(error)
    }
  })
}

// update only if data is no null
const updateCellsInfo = ({id, historyId, cells, chargedCapacity}) => {
  return new Promise((resolve, reject) => {
    try {
      const currentDate = new Date()
      const battery = realm.objects('Battery').filtered(`id == '${id}'`)[0]
      let parsedBattery
      if (battery) {
        parsedBattery = JSON.parse(JSON.stringify(battery))
      } else {
        throw Error('Battery not found')
      }

      const historyList = parsedBattery.history

      if (!historyList) {
        throw Error('There is no history for this battery')
      }

      const histIndex = historyList.findIndex(hist => hist.id === historyId)

      let newHistoryList = [...historyList]

      newHistoryList[histIndex] = {
        ...newHistoryList[histIndex],
        ...(chargedCapacity && {
          chargedCapacity: parseInt(chargedCapacity, 10),
        }),
        ...(cells.length && {
          cells: newHistoryList[histIndex].cells.map((cell, index) => {
            return {
              index: index,
              res: cells[index].res ? cells[index].res : cell.res,
              voltage: cells[index].voltage
                ? cells[index].voltage
                : cell.voltage,
            }
          }),
        }),
        updatedDate: currentDate,
      }

      realm.write(() => {
        resolve((battery.history = newHistoryList))
      })
    } catch (error) {
      reject(error)
    }
  })
}

const addBattery = ({id, brand, model, cellsNb, totalCapacity, discharge}) => {
  return new Promise((resolve, reject) => {
    try {
      const currentDate = new Date()

      realm.write(() => {
        const createdBattery = realm.create('Battery', {
          id: id,
          brand: brand,
          model: model,
          cellsNb: parseInt(cellsNb, 10),
          totalCapacity: parseInt(totalCapacity, 10),
          discharge: parseInt(discharge, 10),
          createdDate: currentDate,
          updatedDate: currentDate,
        })
        resolve(JSON.parse(JSON.stringify(createdBattery)))
      })
    } catch (error) {
      reject(error)
    }
  })
}

const updateBattery = ({
  id,
  brand,
  model,
  totalCapacity,
  discharge,
  notification,
}) => {
  return new Promise((resolve, reject) => {
    try {
      const battery = realm.objects('Battery').filtered(`id == '${id}'`)[0]
      let parsedBattery
      if (battery) {
        parsedBattery = JSON.parse(JSON.stringify(battery))
      } else {
        throw Error('Battery not found')
      }

      const updatedBattery = {
        ...parsedBattery,
        ...(brand && {brand: brand}),
        ...(model && {model: model}),
        ...(totalCapacity && {totalCapacity: parseInt(totalCapacity, 10)}),
        ...(discharge && {discharge: parseInt(discharge, 10)}),
        // Don't update battery updateDate for notifications
        ...(notification
          ? {
              notificationId: notification.id
                ? parseInt(notification.id, 10)
                : notification.id,
              notificationDate: notification.date,
            }
          : {updatedDate: new Date()}),
      }

      realm.write(() => {
        realm.create('Battery', updatedBattery, 'modified')
        resolve(updatedBattery)
      })
    } catch (error) {
      reject(error)
    }
  })
}

const deleteBattery = id => {
  return new Promise((resolve, reject) => {
    try {
      const battery = realm.objects('Battery').filtered(`id == '${id}'`)[0]

      realm.write(() => {
        resolve(realm.delete(battery))
      })
    } catch (error) {
      reject(error)
    }
  })
}

export {
  getBatteries,
  addBattery,
  updateBattery,
  getBattery,
  addCellsInfo,
  updateCellsInfo,
  deleteBattery,
}
