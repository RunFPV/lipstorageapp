import AddBatterySchema from './AddBatterySchema'
import UpdateBatterySchema from './UpdateBatterySchema'

export {AddBatterySchema, UpdateBatterySchema}
