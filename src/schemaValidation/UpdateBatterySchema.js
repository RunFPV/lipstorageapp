import * as Yup from 'yup'
import {genProperties} from '../utils'

const UpdateBatterySchema = parsedCellsNb => {
  return Yup.object().shape({
    chargedCapacity: Yup.number(),
    ...genProperties(
      parsedCellsNb,
      'voltage',
      Yup.number().min(3.2).max(4.2).required(),
    ),
    ...genProperties(parsedCellsNb, 'res', Yup.number().required()),
  })
}

export default UpdateBatterySchema
