import * as Yup from 'yup'

const AddBatterySchema = Yup.object().shape({
  id: Yup.string().required(),
  brand: Yup.string().required(),
  model: Yup.string().required(),
  cellsNb: Yup.number().min(1).required(),
  totalCapacity: Yup.number().min(1).required(),
  discharge: Yup.number().min(1).required(),
})

export default AddBatterySchema
