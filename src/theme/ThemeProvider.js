import React from 'react'
import {colors} from './styles/colors'
import {typography} from './styles/typography'
import {spacing} from './styles/spacing'

export const ThemeContext = React.createContext()

const ThemeProvider = ({isDarkTheme, children}) => {
  const theme = {
    colors: isDarkTheme ? colors.dark : colors.light,
    typography,
    spacing,
  }

  return <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
}

export default ThemeProvider
