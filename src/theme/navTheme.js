import {DarkTheme, DefaultTheme} from '@react-navigation/native'
import {colors} from './styles/colors'

const navDarkTheme = {
  ...DarkTheme,
  colors: {
    ...DarkTheme.colors,
    background: colors.dark.SOFT_BACKGROUND,
    card: colors.dark.BACKGROUND,
  },
}

const navDefaultTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: colors.light.SOFT_BACKGROUND,
    card: colors.light.BACKGROUND,
  },
}

export {navDarkTheme, navDefaultTheme}
