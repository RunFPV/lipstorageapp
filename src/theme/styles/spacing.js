const margin = {
  XS: 5,
  S: 10,
  M: 20,
  L: 30,
}

const radius = {
  S: 2,
  M: 5,
  L: 10,
}

export const spacing = {margin, radius}
