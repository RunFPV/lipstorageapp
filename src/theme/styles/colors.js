const SUN_FLOWER = '#f1c40f'
const ASBESTOS = '#7f8c8d'
const AMETHYST = '#9b59b6'
const EMERALD = '#2ecc71'
const ALIZARIN = '#e74c3c'
const PETER_RIVER = '#3498db'
const WHITE = '#ffffff'
const BLACK = '#000000'
const SOFT_WHITE = '#fbfAf5'
const SOFT_BLACK = '#181a18'

const common = {
  PRIMARY: PETER_RIVER,
  SECONDARY: ASBESTOS,
  TERTIARY: AMETHYST,
  SUCCESS: EMERALD,
  WARNING: SUN_FLOWER,
  ERROR: ALIZARIN,
  WHITE: WHITE,
  BLACK: BLACK,
}

const light = {
  ...common,
  BACKGROUND: WHITE,
  SOFT_BACKGROUND: SOFT_WHITE,
  TEXT: ASBESTOS,
  INVERSED: BLACK,
}

const dark = {
  ...common,
  BACKGROUND: BLACK,
  SOFT_BACKGROUND: SOFT_BLACK,
  TEXT: ASBESTOS,
  INVERSED: WHITE,
}

export const colors = {light, dark}
