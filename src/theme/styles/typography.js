const size = {
  XS: 13,
  S: 16,
  M: 20,
  L: 30,
}

const weight = {
  XS: '100',
  S: '200',
  M: '400',
  L: '700',
  XL: '900',
}

const letterSpacing = {
  S: 2,
  M: 5,
  L: 10,
}

export const typography = {size, letterSpacing, weight}
