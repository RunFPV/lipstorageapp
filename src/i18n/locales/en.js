import {genProperties} from '../../utils'

export default {
  alert: {
    noHistoryTitle: 'No history found !',
    noHistoryText: 'Update battery current cells and capacity before',
    yes: 'Yes',
    no: 'No',
    confirmationTitle: 'Are you sure ?',
    confirmationText: 'Are you sure you want to delete this element ?',
  },
  notification: {
    title: 'Alert battery',
    content: {
      lowVoltage: 'Low voltage',
      highVoltage: 'High voltage',
      since: 'since',
    },
  },
  global: {
    back: 'Back',
    previous: 'Previous',
    next: 'Next',
    save: 'Save',
    cancel: 'Cancel',
    submit: 'Submit',
  },
  language: {
    en: 'English',
    fr: 'French',
  },
  // BatteriesListScreen
  batteriesList: {
    emptyList: 'Click on the plus button to add your first battery',
  },
  // InfoWithCameraScreen
  infoWithCamera: {
    screenTitle: 'Camera scan',
    unauthorized: 'You have to authorize camera to flash Barcodes',
    manualAddBtn: 'Add info manually',
  },
  // BatteryInfoScreen
  batteryInfo: {
    screenTitle: 'Info',
    battery: {
      globalHealth: 'Global health',
      cycles: 'Cycles',
      minCapacityThreshold: 'Min battery capacity',
      fullCapacity: 'Full capacity',
      cellsNumber: 'Cells number',
      maxChargeRate: 'Max charge rate',
      currentVoltage: 'Current voltage',
      lastResistance: 'Last resistance',
      chargedCapacity: 'Charged capacity',
      age: 'Age',
    },
    viewHistory: 'View history',
    delete: 'Delete',
    noDataFound: 'No data found',
    insufficientData: 'Insufficient data',
  },
  // addBatteryScreen
  addBattery: {
    screenTitle: 'Add',
  },
  // updateBatteryScreen
  updateBattery: {
    screenTitle: 'Update',
    updatedDate: 'Last updated date :',
  },
  // SettingsScreen
  settings: {
    screenTitle: 'Settings',
    headerList: {
      global: 'Global settings',
      battery: 'Battery settings',
    },
    language: 'Language',
    rollingNotificationDelay:
      'Delay before battery alert rolling notifications (hours)",',
    battery: {
      minCapacityThreshold: 'Min battery capacity pourcentage',
      maxResThreshold: 'Max battery resistance threshold (mΩ)',
      maxCycleThreshold: 'Max battery cycles threshold',
      maxDayThreshold: 'Max battery days threshold',
      chargeRate: 'Charge rate (c)',
    },
  },
  // HistoryScreen
  history: {
    screenTitle: 'History',
    noHistoryFound: 'No history found...',
    resistanceOverTime: 'Resistance over time',
    cell: 'Cell',
  },
  // Form
  form: {
    id: 'Id',
    brand: 'Brand',
    model: 'Model',
    cellsNb: 'Cells number',
    totalCapacity: 'Capacity (mAh)',
    discharge: 'Discharge (c)',
    chargedCapacity: 'Charged capacity (mAh)',
    ...genProperties(8, 'cellsVoltage', 'Cell voltage', true),
    ...genProperties(8, 'cellsRes', 'Cell resistance', true),
  },
}
