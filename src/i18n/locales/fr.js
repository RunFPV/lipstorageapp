import {genProperties} from '../../utils'

export default {
  alert: {
    noHistoryTitle: 'Aucun historique trouvé !',
    noHistoryText:
      "Mettez d'abord à jour les cellules ou la capacité actuelle de la batterie",
    yes: 'Oui',
    no: 'Non',
    confirmationTitle: 'Êtes vous sur ?',
    confirmationText: 'Êtes vous sur de vouloir supprimer cet élement ?',
  },
  notification: {
    title: 'Alerte batterie',
    content: {
      lowVoltage: 'Tension trop basse',
      highVoltage: 'Tension trop haute',
      since: 'depuis',
    },
  },
  global: {
    back: 'Retour',
    previous: 'Précédent',
    next: 'Suivant',
    save: 'Sauvegarder',
    cancel: 'Annuler',
    submit: 'Valider',
  },
  language: {
    en: 'Anglais',
    fr: 'Français',
  },
  // BatteriesListScreen
  batteriesList: {
    emptyList: 'Cliquer sur le bouton plus pour ajouter une batterie',
  },
  // InfoWithCameraScreen
  infoWithCamera: {
    screenTitle: 'Scan appareil photo',
    unauthorized:
      "Vous devez autoriser l'appareil photo pour flasher des codes barres",
    manualAddBtn: 'Ajouter les infos manuellement',
  },
  // batteryInfoScreen
  batteryInfo: {
    screenTitle: 'Informations',
    battery: {
      globalHealth: 'Santé globale',
      cycles: 'Cycles',
      minCapacityThreshold: 'Capacité minimale de la batterie',
      fullCapacity: 'Capacité totale',
      cellsNumber: 'Nombre de cellules',
      maxChargeRate: 'Charge maximale',
      currentVoltage: 'Tension actuelle',
      lastResistance: 'Dernière resistance',
      chargedCapacity: 'Capacité chargée',
      age: 'Age',
    },
    viewHistory: 'Historique',
    delete: 'Supprimer',
    noDataFound: 'Aucune donnée trouvée',
    insufficientData: 'Données insuffisantes',
  },
  // addBatteryScreen
  addBattery: {
    screenTitle: 'Nouveau',
  },
  // updateBatteryScreen
  updateBattery: {
    screenTitle: 'Mise à jour',
    updatedDate: 'Date de dernière modification :',
  },
  // SettingsScreen
  settings: {
    screenTitle: 'Réglages',
    headerList: {
      global: 'Paramètres globaux',
      battery: 'Paramètres de la batterie',
    },
    language: 'Langue',
    rollingNotificationDelay:
      "Délai avant roulement de notifications d'alertes de batterie (heures)",
    battery: {
      minCapacityThreshold: 'Pourcentage minimal de capacité de la batterie',
      maxResThreshold: 'Seuil maximal de résistance de la batterie (mΩ)',
      maxCycleThreshold: 'Seuil maximal de cycles de la batterie',
      maxDayThreshold: 'Seuil maximal de jours de la batterie',
      chargeRate: 'Taux de charge (c)',
    },
  },
  // HistoryScreen
  history: {
    screenTitle: 'Historique',
    noHistoryFound: 'Aucun historique trouvé...',
    resistanceOverTime: 'Résistance dans le temps',
    cell: 'Cellule',
  },
  // Form
  form: {
    id: 'Id',
    brand: 'Marque',
    model: 'Modèle',
    cellsNb: 'Nb de cellules',
    totalCapacity: 'Capacité (mAh)',
    discharge: 'Décharge (c)',
    chargedCapacity: 'Charged capacity (mAh)',
    ...genProperties(8, 'voltage', 'Tension cellule', true),
    ...genProperties(8, 'res', 'Résistance cellule', true),
  },
}
