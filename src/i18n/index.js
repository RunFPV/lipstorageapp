import I18n from 'i18n-js'
import * as RNLocalize from 'react-native-localize'
// Locales
import en from './locales/en'
import fr from './locales/fr'

const locales = RNLocalize.getLocales()

if (Array.isArray(locales)) {
  I18n.locale = locales[0].languageCode
} else {
  I18n.locale = 'en'
}

I18n.fallbacks = true
I18n.translations = {
  en,
  fr,
}

export default I18n
