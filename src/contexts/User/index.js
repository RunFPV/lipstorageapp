import React, {createContext, useReducer, useEffect} from 'react'
import {reducer, initialState} from './reducer'
import * as a from './actions'
import keys, {getItem, setItem} from '../../asyncStorage'

export const UserContext = createContext({
  state: initialState,
  dispatch: () => null,
})

export const UserProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState)

  // At init, get localStorage value if exists
  useEffect(() => {
    let nextState = initialState
    getItem(keys.USER_PREFS_KEY)
      .then(
        localState => localState && (nextState = {...nextState, ...localState}),
      )
      .catch(error => (nextState = initialState))
      .finally(() => dispatch({type: a.INIT_STATE, value: nextState}))
  }, [])

  // On state Edit, save in localstorage
  useEffect(() => {
    if (state !== initialState) {
      setItem(state, keys.USER_PREFS_KEY).catch(error => console.error(error))
    }
  }, [state])

  return (
    <UserContext.Provider value={[state, dispatch]}>
      {children}
    </UserContext.Provider>
  )
}
