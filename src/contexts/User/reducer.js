import I18n from '../../i18n'
import moment from 'moment/min/moment-with-locales'
import * as a from './actions'

export const initialState = {
  language: I18n.locale,
  rollingNotificationDelay: 24 * 2, // in hours
  battery: {
    minCapacityThreshold: 0.35, // in pourcentage
    chargeRate: 1,
    maxResThreshold: 20, // in mOhm
    maxDayThreshold: 365.25 * 3, // in days
    maxCycleThreshold: 600,
  },
}

export const reducer = (state, action) => {
  switch (action.type) {
    case a.INIT_STATE:
      moment.locale(action.value.language)
      I18n.locale = action.value.language
      return action.value

    case a.UPDATE_LANGUAGE:
      moment.locale(action.value)
      I18n.locale = action.value
      return {
        ...state,
        language: action.value,
      }

    case a.UPDATE_ROLLING_NOTIFICATION_DELAY:
      return {
        ...state,
        language: action.value,
      }

    case a.BATTERY.UPDATE_MIN_CAPACITY_THRESHOLD:
      return {
        ...state,
        battery: {
          ...state.battery,
          minCapacityThreshold: action.value,
        },
      }

    case a.BATTERY.UPDATE_CHARGE_RATE:
      return {
        ...state,
        battery: {
          ...state.battery,
          chargeRate: action.value,
        },
      }

    case a.BATTERY.UPDATE_MAX_RES_THRESHOLD:
      return {
        ...state,
        battery: {
          ...state.battery,
          maxResThreshold: action.value,
        },
      }

    case a.BATTERY.UPDATE_MAX_DAY_THRESHOLD:
      return {
        ...state,
        battery: {
          ...state.battery,
          maxDayThreshold: action.value,
        },
      }

    case a.BATTERY.UPDATE_MAX_CYCLE_THRESHOLD:
      return {
        ...state,
        battery: {
          ...state.battery,
          maxCycleThreshold: action.value,
        },
      }

    default:
      return state
  }
}
