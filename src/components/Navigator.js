import React from 'react'
//Components
import {getFocusedRouteNameFromRoute} from '@react-navigation/native'
import {createNativeStackNavigator} from '@react-navigation/native-stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import * as s from '../screens'
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faCogs, faHome} from '@fortawesome/free-solid-svg-icons'
//Theme
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'

const Tab = createBottomTabNavigator()
const Stack = createNativeStackNavigator()

// Main stack navigator
const MainStackNavigator = ({navigation, route}) => {
  // Hide tabBar for add/update batteries with camera (for android)
  React.useLayoutEffect(() => {
    const tabHiddenRoutes = ['InfosWithCamera', 'AddBattery', 'UpdateBattery']

    if (tabHiddenRoutes.includes(getFocusedRouteNameFromRoute(route))) {
      navigation.setOptions({tabBarStyle: {display: 'none'}})
    } else {
      navigation.setOptions({tabBarStyle: {display: 'flex'}})
    }
  }, [navigation, route])

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="main"
        component={s.BatteriesListScreen}
        options={{title: "Lip'Storage"}}
      />
      {/* BUG: When in modal or fullScreenModal, back button disappears... */}
      {/* TODO: /BUG2:  No rerender on changeLanguage/changeDarkMode for header settings */}
      <Stack.Group>
        <Stack.Screen
          name="InfosWithCamera"
          component={s.InfosWithCameraScreen}
          options={{
            title: I18n.t('infoWithCamera.screenTitle'),
          }}
        />
        <Stack.Screen
          name="AddBattery"
          component={s.AddBatteryScreen}
          options={{title: I18n.t('addBattery.screenTitle')}}
        />
        <Stack.Screen
          name="UpdateBattery"
          component={s.UpdateBatteryScreen}
          options={{title: I18n.t('updateBattery.screenTitle')}}
        />
      </Stack.Group>

      <Stack.Screen
        name="UpdateBatteryNoModal"
        component={s.UpdateBatteryScreen}
        options={{title: I18n.t('updateBattery.screenTitle')}}
      />
      <Stack.Screen
        name="BatteryInfo"
        component={s.BatteryInfoScreen}
        options={{title: I18n.t('batteryInfo.screenTitle')}}
      />
      <Stack.Screen
        name="History"
        component={s.HistoryScreen}
        options={{title: I18n.t('history.screenTitle')}}
      />
    </Stack.Navigator>
  )
}

// Settings stack navigator
const SettingsStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="settings"
        component={s.SettingsScreen}
        options={{title: I18n.t('settings.screenTitle')}}
      />
    </Stack.Navigator>
  )
}

// Main tab navigator
const TabNavigator = () => {
  const theme = useTheme()

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarActiveTintColor: theme.colors.PRIMARY,
        headerShown: false,
        tabBarShowLabel: false,
      }}>
      <Tab.Screen
        name="MainTab"
        component={MainStackNavigator}
        options={{
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} icon={faHome} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="SettingsTab"
        component={SettingsStackNavigator}
        options={{
          tabBarIcon: ({color, size}) => (
            <FontAwesomeIcon color={color} icon={faCogs} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  )
}

const Navigator = TabNavigator

export default Navigator
