import React from 'react'
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native'
import PropTypes from 'prop-types'
// Components
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'

const Card = props => {
  const style = useThemedStyles(styles)
  const theme = useTheme()
  const styleFromProps = {}

  if (props.color) {
    styleFromProps.backgroundColor = props.color
  } else {
    styleFromProps.backgroundColor = theme.colors.PRIMARY
  }

  if (props.radius) {
    styleFromProps.borderRadius = props.radius
  } else {
    styleFromProps.borderRadius = theme.spacing.radius.L
  }

  return (
    <TouchableOpacity
      style={[props.containerStyle, style.container, styleFromProps]}
      onPress={props.onPress}
      disabled={!props.onPress}>
      <View style={style.titleContainer}>
        <FontAwesomeIcon
          icon={props.icon}
          color={theme.colors.WHITE}
          style={style.icon}
        />
        <Text style={style.title}>{props.title}</Text>
      </View>
      <View style={style.content}>{props.children}</View>
    </TouchableOpacity>
  )
}

Card.propTypes = {
  containerStyle: PropTypes.object,
  color: PropTypes.string,
  radius: PropTypes.number,
  icon: PropTypes.object,
  title: PropTypes.string,
  onPress: PropTypes.func,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      width: '100%',
      //   alignItems: 'center',
      //   justifyContent: 'center',
      padding: theme.spacing.margin.S,
    },
    titleContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    icon: {
      marginRight: theme.spacing.margin.XS,
    },
    title: {
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.M,
      fontWeight: theme.typography.weight.L,
    },
    content: {
      margin: theme.spacing.margin.S,
    },
  })

export default Card
