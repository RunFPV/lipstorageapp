import React from 'react'
import {TextInput, View, Text, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'

const FormInput = props => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  // Prevent commas from keyboards (depends of country)
  const commaToDot = text => {
    return props.onChangeText(
      props.keyboardType === 'numeric' ? text.replace(',', '.') : text,
    )
  }

  return (
    <View style={[style.container, props.containerStyle]}>
      <View style={style.header}>
        <Text style={style.title}>
          {props.label} :{' '}
          {props.required && <Text style={style.required}>*</Text>}
        </Text>
        <Text style={style.error}>
          {props.error && props.touched ? props.error : ''}
        </Text>
      </View>
      <TextInput
        value={props.value}
        style={[
          style.input,
          props.style,
          props.error && props.touched && style.inputError,
        ]}
        onChangeText={commaToDot}
        placeholder={props.label}
        placeholderTextColor={theme.colors.TEXT}
        multiline={props.multiline}
        numberOfLines={props.numberOfLines}
        returnKeyType={props.returnKeyType}
        blurOnSubmit={props.returnKeyType && true}
        onSubmitEditing={props.onSubmitEditing}
        onBlur={props.onBlur}
        keyboardType={props.keyboardType}
      />
    </View>
  )
}

FormInput.propTypes = {
  value: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  numberOfLines: PropTypes.number,
  multiline: PropTypes.bool,
  onChangeText: PropTypes.func.isRequired,
  style: PropTypes.object,
  returnKeyType: PropTypes.string,
  onSubmitEditing: PropTypes.func,
  containerStyle: PropTypes.object,
  touched: PropTypes.bool,
  keyboardType: PropTypes.string,
  required: PropTypes.bool,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      paddingVertical: theme.spacing.margin.S,
      flex: 1,
    },
    header: {
      flexDirection: 'column',
    },
    title: {
      color: theme.colors.TEXT,
      fontSize: theme.typography.size.S,
    },
    error: {
      color: theme.colors.ERROR,
    },
    inputError: {
      borderColor: theme.colors.ERROR,
      borderWidth: 1,
    },
    input: {
      borderColor: theme.colors.BACKGROUND,
      borderWidth: 1,
      paddingLeft: 10,
      color: theme.colors.INVERSED,
      backgroundColor: theme.colors.BACKGROUND,
      height: 40,
      fontSize: theme.typography.size.S,
      borderRadius: theme.spacing.radius.L,
    },
    required: {
      color: theme.colors.ERROR,
      fontWeight: theme.typography.weight.L,
    },
  })

export default FormInput
