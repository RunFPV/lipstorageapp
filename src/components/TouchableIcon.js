import React from 'react'
import {TouchableOpacity, StyleSheet} from 'react-native'
import PropTypes from 'prop-types'
// Components
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
// Theming
import useThemedStyles from '../theme/useThemedStyles'

const TouchableIcon = props => {
  const style = useThemedStyles(styles)

  return (
    <TouchableOpacity
      style={[props.style, style.container]}
      onPress={props.onPress}>
      <FontAwesomeIcon
        size={20}
        icon={props.icon}
        color={props.color ? props.color : style.iconColor.color}
      />
    </TouchableOpacity>
  )
}

TouchableIcon.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.object.isRequired,
  color: PropTypes.string,
  style: PropTypes.object,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      height: 30,
      width: 30,
      // flex: 1,
      // padding: 15,
      justifyContent: 'center',
      alignItems: 'flex-end',
    },
    iconColor: {
      color: theme.colors.SECONDARY,
    },
  })

export default TouchableIcon
