import React, {useEffect, useState} from 'react'
import {View, Button, StyleSheet, Platform} from 'react-native'
import PropTypes from 'prop-types'
// Components
import {Picker} from '@react-native-picker/picker'
import Modal from 'react-native-modal'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'

const ModalPicker = props => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  const [selectedValue, setselectedValue] = useState()

  useEffect(() => {
    setselectedValue(props.currentItemValue)
  }, [props.currentItemValue])

  return (
    <Modal
      isVisible={props.isVisible}
      style={style.container}
      backdropOpacity={0.25}>
      <View style={style.subContainer}>
        <View style={style.btnContainer}>
          <Button title={I18n.t('global.cancel')} onPress={props.toggle} />
          <Button
            title={I18n.t('global.save')}
            onPress={() => props.save(selectedValue)}
          />
        </View>

        {props.items && (
          <Picker
            selectedValue={selectedValue}
            onValueChange={setselectedValue}
            style={style.pickerStyle}>
            {props.items.map((item, key) => {
              return (
                <Picker.Item
                  key={key}
                  label={item.label}
                  value={item.value}
                  color={theme.colors.INVERSED}
                />
              )
            })}
          </Picker>
        )}
      </View>
    </Modal>
  )
}

ModalPicker.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  currentItemValue: PropTypes.any,
  save: PropTypes.func.isRequired,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      margin: 0,
      justifyContent: 'flex-end',
    },
    subContainer: {
      backgroundColor: theme.colors.BACKGROUND,
      padding: Platform.OS === 'android' ? theme.spacing.margin.S : 0,
    },
    pickerStyle: {
      margin: Platform.OS === 'android' ? theme.spacing.margin.S : 0,
    },
    btnContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: theme.spacing.margin.S,
    },
  })

export default ModalPicker
