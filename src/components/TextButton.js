import React from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  ActivityIndicator,
} from 'react-native'
import PropTypes from 'prop-types'
// Components
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'

const TextButton = props => {
  const style = useThemedStyles(styles)
  const theme = useTheme()
  const styleFromProps = {}

  if (props.color) {
    styleFromProps.backgroundColor = props.color
  } else {
    styleFromProps.backgroundColor = theme.colors.PRIMARY
  }

  if (props.radius) {
    styleFromProps.borderRadius = props.radius
  } else {
    styleFromProps.borderRadius = theme.spacing.radius.L
  }

  return (
    <View
      style={[
        props.containerStyle,
        style.mainContainer,
        styleFromProps,
        props.disabled || (props.loading && style.disabled),
      ]}>
      <TouchableOpacity
        style={[style.container]}
        onPress={props.onPress}
        disabled={props.disabled || props.loading}>
        {props.loading ? (
          <ActivityIndicator color={theme.colors.WHITE} />
        ) : (
          <>
            {props.icon && (
              <FontAwesomeIcon
                style={style.icon}
                icon={props.icon}
                color={theme.colors.WHITE}
              />
            )}
            <Text style={style.text}>{props.text}</Text>
          </>
        )}
      </TouchableOpacity>
    </View>
  )
}

TextButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  text: PropTypes.string.isRequired,
  color: PropTypes.string,
  radius: PropTypes.number,
  disabled: PropTypes.bool,
  icon: PropTypes.object,
  loading: PropTypes.bool,
}

const styles = theme =>
  StyleSheet.create({
    mainContainer: {
      height: 50,
      flex: 1,
    },
    container: {
      height: '100%',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
    },
    icon: {
      marginRight: theme.spacing.margin.S,
    },
    text: {
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.M,
      fontWeight: theme.typography.weight.M,
    },
    disabled: {
      opacity: 0.5,
    },
  })

export default TextButton
