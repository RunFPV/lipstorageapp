import React from 'react'
import {StyleSheet, Text, View} from 'react-native'
import PropTypes from 'prop-types'
// Components
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'

const CardItem = props => {
  const style = useThemedStyles(styles)
  const theme = useTheme()

  const iconContainerStyle = {width: props.iconSize ? props.iconSize + 10 : 17}

  const containerStyle = {
    flexDirection: props.rightIcon ? 'row-reverse' : 'row',
  }

  return (
    <View style={[props.styleContainer, style.container, containerStyle]}>
      {props.icon && (
        <View style={iconContainerStyle}>
          <FontAwesomeIcon
            icon={props.icon}
            size={props.iconSize ? props.iconSize : 8}
            color={theme.colors.WHITE}
          />
        </View>
      )}
      <Text style={[style.cardText, props.centerContent && style.center]}>
        {props.text}
      </Text>
    </View>
  )
}

CardItem.propTypes = {
  rightIcon: PropTypes.bool,
  text: PropTypes.string.isRequired,
  icon: PropTypes.object,
  iconSize: PropTypes.number,
  styleContainer: PropTypes.object,
  centerContent: PropTypes.bool,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      alignItems: 'center',
      flexBasis: '50%',
      justifyContent: 'center',
      borderRadius: theme.spacing.radius.L,
      flex: 1,
    },
    cardText: {
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.M,
    },
    center: {
      flex: 1,
    },
  })

export default CardItem
