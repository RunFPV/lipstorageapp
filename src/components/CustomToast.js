import React from 'react'
import {StyleSheet} from 'react-native'
import Toast, {ErrorToast} from 'react-native-toast-message'
// Theming
import useThemedStyles from '../theme/useThemedStyles'
import useTheme from '../theme/useTheme'

const CustomToast = () => {
  const theme = useTheme()
  const style = useThemedStyles(styles)

  const toastConfig = {
    // Overwrite 'success' type, by modifying the existing `BaseToast` component
    //   success: (props) => (
    //     <BaseToast
    //       {...props}
    //       style={{ borderLeftColor: 'pink' }}
    //       contentContainerStyle={{ paddingHorizontal: 15 }}
    //       text1Style={{
    //         fontSize: 15,
    //         fontWeight: '400'
    //       }}
    //     />
    //   ),

    // Overwrite 'error' type, by modifying the existing `ErrorToast` component
    error: props => (
      <ErrorToast
        {...props}
        text2NumberOfLines={2}
        text1Style={{
          fontSize: theme.typography.size.S,
          color: theme.colors.INVERSED,
        }}
        text2Style={{fontSize: theme.typography.size.XS}}
        contentContainerStyle={style.contentContainer}
      />
    ),
  }

  return <Toast config={toastConfig} />
}

const styles = theme =>
  StyleSheet.create({
    contentContainer: {
      backgroundColor: theme.colors.SOFT_BACKGROUND,
      borderTopRightRadius: 5,
      borderBottomRightRadius: 5,
    },
  })

export default CustomToast
