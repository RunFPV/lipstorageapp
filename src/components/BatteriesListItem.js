import React from 'react'
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native'
import PropTypes from 'prop-types'
// Theming
import useThemedStyles from '../theme/useThemedStyles'

const BatteriesListItem = props => {
  const style = useThemedStyles(styles)

  return (
    <TouchableOpacity style={style.container} onPress={props.onPress}>
      <View style={style.leftContainer}>
        <Text numberOfLines={1} style={style.text}>
          {props.brand}
        </Text>
        <Text numberOfLines={1} style={[style.text, style.subText]}>
          {props.model}
        </Text>
      </View>
      <View style={style.midContainer}>
        <Text style={style.text}>{props.id}</Text>
      </View>
      <View style={style.rightContainer}>
        <View style={style.circleContainer}>
          <Text style={style.rightText}>{props.averageVoltage}</Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

BatteriesListItem.propTypes = {
  onPress: PropTypes.func.isRequired,
  averageVoltage: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  model: PropTypes.string.isRequired,
  brand: PropTypes.string.isRequired,
}

const styles = theme =>
  StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      borderRadius: theme.spacing.radius.M,
      backgroundColor: theme.colors.PRIMARY,
      padding: theme.spacing.margin.S,
      alignItems: 'center',
    },
    leftContainer: {
      justifyContent: 'center',
      flex: 1,
    },
    rightContainer: {
      flex: 1,
      alignItems: 'flex-end',
    },
    circleContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 45 / 2,
      borderColor: theme.colors.WHITE,
      borderWidth: 1,
      width: 45,
      height: 45,
    },
    rightText: {
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.M,
    },
    midContainer: {
      justifyContent: 'center',
      alignItems: 'center',
    },
    text: {
      color: theme.colors.WHITE,
      fontSize: theme.typography.size.M,
    },
    subText: {
      fontSize: theme.typography.size.S,
      fontWeight: theme.typography.weight.S,
    },
  })

export default BatteriesListItem
