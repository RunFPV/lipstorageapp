import React from 'react'
import PropTypes from 'prop-types'
// Components
import {faCheck} from '@fortawesome/free-solid-svg-icons'
import FormInput from '../components/FormInput'
import TextButton from '../components/TextButton'
import {Formik} from 'formik'
import Separator from './Separator'
// Theming
import useTheme from '../theme/useTheme'
// Internationalization
import I18n from '../i18n'

const Form = props => {
  const theme = useTheme()

  return (
    <Formik
      validationSchema={props.validationSchema}
      initialValues={props.formValues}
      onSubmit={props.onFormSubmit}>
      {({handleChange, handleBlur, handleSubmit, values, errors, touched}) => (
        <>
          {Object.entries(props.formValues).map(([key]) => {
            // Don't print key input
            if (key === 'id' || key === 'historyId') {
              return
            }

            const keyboardType =
              key === 'brand' || key === 'model' ? 'default' : 'numeric'

            const returnKeyType =
              key === 'brand' || key === 'model' ? 'default' : 'done'

            // TODO: work on print display (2 inputs in one row or 1 title per type of input)
            // TODO: Pass to next input with keyboard, submit form with keyboard
            return (
              <FormInput
                key={key}
                label={I18n.t(`form.${key}`)}
                value={values[key]}
                error={errors[key]}
                touched={touched[key]}
                onBlur={handleBlur(key)}
                keyboardType={keyboardType}
                onChangeText={handleChange(key)}
                returnKeyType={returnKeyType}
                required={key !== 'chargedCapacity'}
              />
            )
          })}
          <Separator size="S" />
          <TextButton
            text={I18n.t('global.submit')}
            icon={faCheck}
            color={theme.colors.SUCCESS}
            loading={props.loading}
            onPress={handleSubmit}
          />
        </>
      )}
    </Formik>
  )
}

Form.propTypes = {
  onFormSubmit: PropTypes.func.isRequired,
  formValues: PropTypes.object.isRequired,
  validationSchema: PropTypes.object,
  loading: PropTypes.bool,
}

export default Form
