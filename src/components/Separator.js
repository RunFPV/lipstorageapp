import React from 'react'
import PropTypes from 'prop-types'
// Components
import {View} from 'react-native'
//Theming
import useTheme from '../theme/useTheme'

const Separator = props => {
  const theme = useTheme()
  const styleFromProps = {}

  if (props.size) {
    styleFromProps.marginVertical = theme.spacing.margin[props.size]
  } else {
    styleFromProps.marginVertical = theme.spacing.margin.XS
  }

  return <View style={styleFromProps} />
}

Separator.propTypes = {
  size: PropTypes.string,
}

export default Separator
