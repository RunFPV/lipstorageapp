import AsyncStorage from '@react-native-async-storage/async-storage'
import * as keys from './keys'

const setItem = (data, key) => {
  return new Promise((resolve, reject) => {
    const stringifiedState = JSON.stringify(data)
    AsyncStorage.setItem(key, stringifiedState).then(resolve).catch(reject)
  })
}

const getItem = key => {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem(key).then(localState => {
      if (localState) {
        try {
          resolve(JSON.parse(localState))
        } catch (error) {
          reject(error)
        }
      } else {
        resolve(null)
      }
    })
  })
}

export default keys

export {setItem, getItem}
