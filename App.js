/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react'
import {StatusBar, useColorScheme} from 'react-native'
// Splashscreen
import SplashScreen from 'react-native-splash-screen'
// Components
import {UserProvider} from './src/contexts/User'
import Navigator from './src/components/Navigator'
import {NavigationContainer} from '@react-navigation/native'
import CustomToast from './src/components/CustomToast'
import {Notifications} from 'react-native-notifications'
// Theming
import ThemeProvider from './src/theme/ThemeProvider'
import {navDarkTheme, navDefaultTheme} from './src/theme/navTheme'
// Database
import realm from './src/database/realm'
const App = () => {
  useEffect(() => {
    SplashScreen.hide()
    realm.schemaVersion
  }, [])

  Notifications.registerRemoteNotifications()

  Notifications.events().registerNotificationReceivedForeground(
    (notification, completion) =>
      completion({alert: false, sound: false, badge: false}),
  )

  Notifications.events().registerNotificationOpened(
    (notification, completion) => completion(),
  )

  const isDarkTheme = useColorScheme() === 'dark'

  return (
    <UserProvider>
      <ThemeProvider isDarkTheme={isDarkTheme}>
        <StatusBar barStyle={isDarkTheme ? 'light-content' : 'dark-content'} />
        <NavigationContainer
          theme={isDarkTheme ? navDarkTheme : navDefaultTheme}>
          <Navigator />
        </NavigationContainer>
        <CustomToast />
      </ThemeProvider>
    </UserProvider>
  )
}

export default App
