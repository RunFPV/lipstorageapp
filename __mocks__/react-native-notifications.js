// jest.mock('react-native-notifications', () => {
//   return
// })

jest.mock('react-native-notifications', () => ({
  __esModule: true, // this property makes it work
  default: 'mockedDefaultExport',
  Notifications: {
    registerRemoteNotifications: jest.fn(),
    events: () => {
      return {
        registerNotificationReceivedForeground: jest.fn(),
        registerNotificationOpened: jest.fn(),
      }
    },
  },
}))
